﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Films.Models;

namespace Films.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            FilmContext db = new FilmContext();
            if (ModelState.IsValid)
            {
                User user = null;

                user = db.Users.FirstOrDefault(u => u.Email == model.Name && u.Password == model.Password);


                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(model.Name, true);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Пользователь с таким логином и паролем не существует");
                }
            }
            return View(model);
        }

        public ActionResult Register()
        {
            //if (User.Identity.IsAuthenticated)
            //{
                //return RedirectToAction("Login");
            //}
            //else
            //{
            return View();
            //}
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            FilmContext db = new FilmContext();

            if (ModelState.IsValid)
            {
                User user = null;

                user = db.Users.FirstOrDefault(u => u.Email == model.Name);

                if (user == null)
                {

                    db.Users.Add(new User
                        {Email = model.Name, Password = model.Password, Age = model.Age, RoleId = 2});
                    db.SaveChanges();

                    user = db.Users.Where(u => u.Email == model.Name && u.Password == model.Password)
                        .FirstOrDefault();


                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(model.Name, true);
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Пользовтель с таким логином уже существует");
                }
            }
            return View(model);
        }




    }
}