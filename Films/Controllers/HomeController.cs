﻿using Films.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Films.Controllers
{
    public class HomeController : Controller
    {
        FilmContext db = new FilmContext();

        public ActionResult Index(int page = 1)
        {
            var films = db.FilmsDb;
            var posters = db.Posters;
            ViewBag.PostersDb = posters;
            string name = User.Identity.Name;
            if (User.Identity.IsAuthenticated)
            {
                User user = db.Users.FirstOrDefault(u => u.Email == name);
                ViewBag.IdUser = user.Id;
            }


            int pageSize = 3; // количество объектов на страницу
            IEnumerable<FilmDb> filmsPerPages = films.OrderBy(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = films.Count() };
            IndexViewModel ivm = new IndexViewModel { PageInfo = pageInfo, Films = filmsPerPages };
            return View(ivm);
        }

        public ActionResult AllUsers()
        {
            //string result = "Вы не авторизованы";
            //if (User.Identity.IsAuthenticated)
            //{
                var users = db.Users;
                ViewBag.Users = users;
            //}
            return View();
        }

        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult AddNewFilm()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddNewFilm(FilmDb film, HttpPostedFileBase uploadPosters)
        {
            if (User.Identity.IsAuthenticated)
            {
                var newId = 0;
                if (ModelState.IsValid && uploadPosters != null)
                {
                    string fileName = Path.GetFileName(uploadPosters.FileName);
                    string filePath = "~/Content/Posters/" + film.Name + fileName;
                    uploadPosters.SaveAs(Server.MapPath(filePath));

                    Poster poster = new Poster();

                    poster.Image = filePath;

                    db.Posters.Add(poster);
                    db.SaveChanges();

                    newId = poster.Id;
                }
                else if (ModelState.IsValid && uploadPosters == null)
                {
                    Poster emptyPoster = new Poster();
                    emptyPoster = db.Posters.FirstOrDefault(p => p.Image == "~/Content/Posters/empty.jpg");
                    newId = emptyPoster.Id;
                }

                User user = null;
                string nameUser= User.Identity.Name;
                user = db.Users.FirstOrDefault(u => u.Email == nameUser);

                film.IdUser = user.Id;
                film.IdPoster = newId;

                db.FilmsDb.Add(film);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            string nameUser = User.Identity.Name;
            User idUser = db.Users.FirstOrDefault(u => u.Email == nameUser);
            Role role = db.Roles.FirstOrDefault(r => r.Name == "admin");
            FilmDb film = db.FilmsDb.Find(id);

            if (film != null && (idUser.Id == film.IdUser || idUser.Roles.Contains(role)))
            {
                Poster poster = db.Posters.FirstOrDefault(p => p.Id == film.IdPoster);

                ViewBag.Film = film;
                ViewBag.Poster = poster;

                string name = User.Identity.Name;
                User user = db.Users.FirstOrDefault(u => u.Email == name);
                ViewBag.IdUser = user.Id;

                return View(film);
            }

            string url = Url.Content("~/Home/ViewFilm/" + id);

            return Redirect(url);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(FilmDb film, HttpPostedFileBase uploadPosters, bool delitPoster)
        {
            string nameUser = User.Identity.Name;
            User idUser = db.Users.FirstOrDefault(u => u.Email == nameUser);
            Role role = db.Roles.FirstOrDefault(r => r.Name == "admin");
            bool adm = idUser.Roles.Contains(role);

            if (idUser.Id == film.IdUser || adm)
            {
                FilmDb edFilm = new FilmDb();
                edFilm = db.FilmsDb.FirstOrDefault(f => f.Id == film.Id);

                var newId = 0;
                if (ModelState.IsValid && uploadPosters != null && !delitPoster)
                {
                    string fileName = Path.GetFileName(uploadPosters.FileName);
                    string filePath = "~/Content/Posters/" + film.Name + fileName;
                    uploadPosters.SaveAs(Server.MapPath(filePath));

                    Poster poster = new Poster();

                    poster.Image = filePath;

                    db.Posters.Add(poster);
                    db.SaveChanges();

                    newId = poster.Id;
                }
                else if(delitPoster)
                {
                    Poster emptyPoster = new Poster();

                    emptyPoster = db.Posters.FirstOrDefault(p => p.Image == "~/Content/Posters/empty.jpg");
                    newId = emptyPoster.Id;
                } else if (uploadPosters == null && !delitPoster)
                {
                    newId = edFilm.IdPoster;
                }

                edFilm.Date = film.Date;
                edFilm.Description = film.Description;
                edFilm.IdPoster = newId;
                edFilm.Name = film.Name;
                edFilm.Producer = film.Producer;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ViewFilm(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            FilmDb film = db.FilmsDb.Find(id);
            if (film != null)
            {
                string nameUser = User.Identity.Name;
                User idUser = db.Users.FirstOrDefault(u => u.Email == nameUser);

                Poster poster = db.Posters.FirstOrDefault(p => p.Id == film.IdPoster);

                ViewBag.Film = film;
                ViewBag.Poster = poster;
                if (idUser != null)
                {
                    ViewBag.IdUser = idUser.Id;
                }


                return View(film);
            }

            return HttpNotFound();
        }
    }
}