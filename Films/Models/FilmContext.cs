﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Films.Models
{
    public class FilmContext : DbContext
    {
        public DbSet<Poster> Posters { get; set; }
        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<FilmDb> FilmsDb { get; set; }
    }

    public class FilmDbInitializer : DropCreateDatabaseAlways<FilmContext>
    {
        protected override void Seed(FilmContext db)
        {
            Role admin = new Role {Name = "admin"};
            Role user = new Role {Name = "user"};
            db.Roles.Add(admin);
            db.Roles.Add(user);
            db.Users.Add(new User
            {
                Email = "somemail@gmail.com",
                Password = "123456",
                Roles = {admin}
            });

            db.SaveChanges();
            //base.Seed(db);
        }
    }
}