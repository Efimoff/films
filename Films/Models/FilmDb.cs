﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Films.Models
{
    public class FilmDb
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int IdUser { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Дата выхода")]
        public string Date { get; set; }

        [Display(Name = "Продюсер")]
        public string Producer { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int IdPoster { get; set; }
    }

    public class Poster
    {
        public int Id { get; set; }
        public string Image { get; set; }
    }
}